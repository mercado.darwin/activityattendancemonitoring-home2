<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use \App\Activity;
use \App\Job;
use Auth;
use Session;

class UserController extends Controller
{
	public function index(){
    	$users = User::all();

    	$activities = Activity::all();

    	$usersWithActivities = $activities->pluck('user_id')->toArray();

    	return view('adminviews.users', compact('users', 'usersWithActivities'));
    }

    public function changeStatus($id){
    	$user = User::find($id);

    	if ($user->status_id == 2) {
    		$user->status_id = 1;    		
    	}else{
    		$user->status_id = 2;    		
    	}
    	$user->save();
    	return redirect('/allusers');
    }

    public function changeRole($id){
    	$user = User::find($id);

    	if ($user->role_id == 2) {
    		$user->role_id = 1;
    		
    	}else{
    		$user->role_id = 2;    		
    	}
    	$user->save();
    	return redirect('/allusers');
    }

    public function destroy($id){
    	$user = User::find($id);
    	$user->delete();

    	Session::flash('message', "$user->name has been deleted");
    	return redirect()->back();
    }

    public function applyJob(){
        $jobs = Job::all();

        return view('userviews.applyforjob', compact('jobs'));
    }

    public function storeUserJob(Request $req){
        $user = Auth::user()->id;
        $jobs = Job::find($req->input('job_id'));

        $jobs->users()->attach($user);

        $jobs->save();

        return redirect('/bulletin');
    }

    public function showUserJobs(Request $req){
        $users = User::all();     

        return view('adminviews.userjobs', compact('users'));
    }

    public function destroyAttendee($id, Request $req){
        $user = User::find($id);
        $activities = Activity::find($req->input('activity_id'));

        $activities->users()->detach($user);

        $activities->save();

        Session::flash('message', "$user->name has been deleted");
        return redirect()->back();
    }
}
