@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Announcements</h1>
<div class="col-lg-10 offset-lg-1">
	<a href="/addannouncement" class="btn btn-info">Add</a>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Announcements</th>
				<th>Actions</th>
				
			</tr>
		</thead>
		<tbody>
			@foreach($announcements as $announcement)
			<tr>
				<th>{{$announcement->announcement}}</th>
				<th>
					<form action="/posttobulletin/{{$announcement->id}}" method="POST">
						@csrf
						@method('PATCH')

						@if($announcement->status == 2)
							<button type="submit" class="btn btn-success">Post</button>
						@else
							<button type="submit" class="btn btn-warning">Hide</button>
						@endif
						
					</form>
					<a href="/editannouncement/{{$announcement->id}}" class="btn btn-info">Edit</a>				
						<form action="/deleteannouncement/{{$announcement->id}}" method="POST">
							@csrf
							@method('DELETE')
							<button type="submit" class="btn btn-danger">Delete</button>
						</form>
					
				
				</th>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection