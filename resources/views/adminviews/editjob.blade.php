@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Edit Jobs

<div class="col-lg-4 offset-lg-4">
	<form action="/editjob/{{$job->id}}" method="POST">
		@csrf
		@method('PATCH')
		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control" value="{{$job->title}}">
		</div>
		<div class="form-group">
			<label for="description">Description</label>
			<input type="text" name="description" class="form-control" value="{{$job->description}}">
		</div>
		<button class="btn btn-warning" type="submit">Update</button>
	</form>
</div>
@endsection