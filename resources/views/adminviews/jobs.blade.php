@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Jobs</h1>
<div class="col-lg-10 offset-lg-2">
	<a href="/addjob" class="btn btn-success">Add Jobs</a>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Job Title</th>
				<th>Descriptions</th>
				<th>Actions</th>				
			</tr>
		</thead>
		<tbody>
			@foreach($jobs as $job)
			<tr>
				<th>{{$job->title}}</th>
				<th>{{$job->description}}</th>
				<th>
					<a href="/editjob/{{$job->id}}" class="btn btn-info">Edit</a>
					<form action="/deletejob/{{$job->id}}" method="POST">
						@csrf
						@method('DELETE')
						<button type="submit" class="btn btn-danger">Delete</button>
					</form>
					
				</th>
			</tr>			
			@endforeach
		</tbody>
	</table>
</div>
@endsection