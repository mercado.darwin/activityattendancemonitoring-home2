@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Add Jobs Form</h1>

<div class="col-lg-4 offset-lg-4">
	<form action="/addjob" method="POST" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label for="title">Name</label>
			<input type="text" name="title" class="form-control">
		</div>
		<div class="form-group">
			<label for="description">Description</label>
			<input type="text" name="description" class="form-control">
		</div>
		<button class="btn btn-warning" type="submit">Add Job</button>
	</form>
</div>
@endsection