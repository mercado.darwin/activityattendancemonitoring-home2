@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Activities</h1>
<div class="col-lg-10 offset-lg-1">
	<table class="table table-striped">
		<thead>
			<tr>
				<th></th>
				<th></th>
				<th><a href="/addactivity" class="btn btn-success">Add Activity</a></th>			
			</tr>
		</thead>
		<tbody>
			@foreach($activities as $activity)
			<tr>
				<th><img class="card-img-top img img-responsive full-width" src="{{asset($activity->imgPath)}}" alt="Nothing"></th>
				<th>
					<h1>{{$activity->title}}</h1>
					<p>{{$activity->description}}</p>
					<p>{{$activity->date}}</p>
					<p>{{$activity->venue}}</p>
					<p><a href="/showattendees/{{$activity->id}}" class="btn btn-info">Show Attendees</a></p>
				</th>
				<th>
					<a href="/editactivity/{{$activity->id}}" class="btn btn-info">Edit</a>
					@if(in_array($activity->id, $activitiesWithUsers))
					@else
					<form action="/deleteactivity/{{$activity->id}}" method="POST">
						@csrf
						@method('DELETE')
						<button type="submit" class="btn btn-danger">Delete</button>
					</form>
					@endif
					
				</th>
			</tr>			
			@endforeach
		</tbody>
	</table>
</div>
@endsection